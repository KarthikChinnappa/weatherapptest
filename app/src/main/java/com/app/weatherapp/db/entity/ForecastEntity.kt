package com.app.weatherapp.db.entity

import android.os.Parcelable
import androidx.room.*
import com.app.weatherapp.domain.model.ForecastResponse
import com.app.weatherapp.domain.model.ListItem
import com.app.weatherapp.domain.model.WeatherItem
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "Forecast")
data class ForecastEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int,

    @Embedded
    var city: CityEntity?,

    @ColumnInfo(name = "list")
    var weather: List<ListItem>?
) : Parcelable {

    @Ignore
    constructor(forecastResponse: ForecastResponse) : this(
        id = 0,
        city = forecastResponse.city?.let { CityEntity(it) },
        weather = forecastResponse.list
    )
}
