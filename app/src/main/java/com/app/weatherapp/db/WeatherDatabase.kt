package com.app.weatherapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.weatherapp.constants.Constants.DB.DB_VERSION
import com.app.weatherapp.db.dao.CurrentWeatherDao
import com.app.weatherapp.db.dao.ForecastDao
import com.app.weatherapp.db.entity.CurrentWeatherEntity
import com.app.weatherapp.db.entity.ForecastEntity

@Database(
    entities = [
        ForecastEntity::class,
        CurrentWeatherEntity::class],
    version = DB_VERSION
)
@TypeConverters(DataConverter::class)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun forecastDao(): ForecastDao

    abstract fun currentWeatherDao(): CurrentWeatherDao
}
