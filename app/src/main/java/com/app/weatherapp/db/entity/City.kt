package com.app.weatherapp.db.entity

data class City(
	val city: String,
	val accentcity: String,
	val latitude: Double,
	val longitude: Double
)