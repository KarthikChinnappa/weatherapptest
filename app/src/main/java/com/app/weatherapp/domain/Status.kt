package com.app.weatherapp.domain

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
