package com.app.weatherapp.base

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()
