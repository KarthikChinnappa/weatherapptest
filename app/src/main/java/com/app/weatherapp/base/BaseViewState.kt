package com.app.weatherapp.base

import com.app.weatherapp.domain.Status

open class BaseViewState(private val baseStatus: Status, private val baseError: String?) {
    fun isLoading() = baseStatus == Status.LOADING
    fun getErrorMessage() = baseError
    fun shouldShowErrorMessage() = baseError != null
}
