package com.app.weatherapp.di.module

import com.app.weatherapp.ui.location.LocationFragment
import com.app.weatherapp.ui.splash.SplashFragment
import com.app.weatherapp.ui.weather.WeatherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributeWeatherSearchFragment(): LocationFragment

    @ContributesAndroidInjector
    abstract fun contributeWeatherFragment(): WeatherFragment

}
