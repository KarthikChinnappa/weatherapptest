package com.app.weatherapp.di.component

import android.app.Application
import com.app.weatherapp.ClimateApp
import com.app.weatherapp.di.module.ActivityModule
import com.app.weatherapp.di.module.ApplicationModule
import com.app.weatherapp.di.module.DatabaseModule
import com.app.weatherapp.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        ActivityModule::class,
        ViewModelModule::class
    ]
)

interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(climateApp: ClimateApp)
}
