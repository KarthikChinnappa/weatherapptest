package com.app.weatherapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.weatherapp.di.ViewModelFactory
import com.app.weatherapp.di.key.ViewModelKey
import com.app.weatherapp.ui.main.MainActivityViewModel
import com.app.weatherapp.ui.location.LocationViewModel
import com.app.weatherapp.ui.splash.SplashFragmentViewModel
import com.app.weatherapp.ui.weather.WeatherFragmentViewModel
import com.app.weatherapp.ui.weather.forecast.ForecastItemViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @IntoMap
    @Binds
    @ViewModelKey(SplashFragmentViewModel::class)
    abstract fun provideSplashFragmentViewModel(splashFragmentViewModel: SplashFragmentViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun provideMainViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(LocationViewModel::class)
    abstract fun provideLocationViewModel(locationViewModel: LocationViewModel): ViewModel

   @IntoMap
    @Binds
    @ViewModelKey(WeatherFragmentViewModel::class)
    abstract fun provideWeatherFragmentViewModel(weatherFragmentViewModel: WeatherFragmentViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(ForecastItemViewModel::class)
    abstract fun provideForecastItemViewModel(forecastItemViewModel: ForecastItemViewModel): ViewModel

}
