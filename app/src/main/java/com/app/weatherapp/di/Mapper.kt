package com.app.weatherapp.di

interface Mapper<R, D> {
    fun mapFrom(type: R): D
}
