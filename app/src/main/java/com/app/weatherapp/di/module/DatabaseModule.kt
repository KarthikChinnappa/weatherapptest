package com.app.weatherapp.di.module

import android.content.Context
import androidx.room.Room
import com.app.weatherapp.constants.Constants
import com.app.weatherapp.db.WeatherDatabase
import com.app.weatherapp.db.dao.CurrentWeatherDao
import com.app.weatherapp.db.dao.ForecastDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun getDatabase(context: Context): WeatherDatabase {
        return Room.databaseBuilder(
            context,
            WeatherDatabase::class.java, Constants.DB.DB_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun provideForecastDao(db: WeatherDatabase): ForecastDao {
        return db.forecastDao()
    }

    @Singleton
    @Provides
    fun provideCurrentWeatherDao(db: WeatherDatabase): CurrentWeatherDao {
        return db.currentWeatherDao()
    }

}
