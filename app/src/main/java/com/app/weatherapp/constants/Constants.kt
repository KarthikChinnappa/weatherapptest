package com.app.weatherapp.constants

object Constants {

    object NetworkKey {
        const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
        const val API_KEY_VALUE = "7822af937bc7b4ba65284d39e6007880"
        const val RATE_LIMITER_TYPE = "data"
        const val API_KEY_QUERY = "appid"
    }

    object DB{
        const val DB_NAME="ClimateApp-DB"
        const val DB_VERSION=1
    }

    object Coordinates {
        const val LAT = "lat"
        const val LON = "lon"
        const val METRIC = "metric"
    }
}
