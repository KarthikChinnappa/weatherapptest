package com.app.weatherapp.ui.splash

import android.content.SharedPreferences
import com.app.weatherapp.base.BaseViewModel
import javax.inject.Inject

class SplashFragmentViewModel @Inject constructor(var sharedPreferences: SharedPreferences) : BaseViewModel() {
    var navigateDashboard = false
}
