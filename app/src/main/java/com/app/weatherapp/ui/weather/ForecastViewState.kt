package com.app.weatherapp.ui.weather

import com.app.weatherapp.base.BaseViewState
import com.app.weatherapp.db.entity.ForecastEntity
import com.app.weatherapp.domain.Status

class ForecastViewState(
    val status: Status,
    val error: String? = null,
    val data: ForecastEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}
