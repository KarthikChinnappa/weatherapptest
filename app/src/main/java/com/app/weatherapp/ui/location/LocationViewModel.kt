package com.app.weatherapp.ui.location

import android.content.SharedPreferences
import com.app.weatherapp.ClimateApp
import com.app.weatherapp.base.BaseViewModel
import com.app.weatherapp.constants.Constants
import com.app.weatherapp.db.entity.City
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject


class LocationViewModel @Inject internal constructor(private val pref: SharedPreferences) :
    BaseViewModel() {

    fun saveCoordsToSharedPref(city: City): Single<String>? {
        return Single.create<String> {
            pref.edit().putString(Constants.Coordinates.LAT, city.latitude.toString()).apply()
            pref.edit().putString(Constants.Coordinates.LON, city.longitude.toString()).apply()
            it.onSuccess("")
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }


    fun getTopCities(): ArrayList<City> =
        Gson().fromJson(getCityJSON(), object : TypeToken<ArrayList<City>>() {}.type)

    private fun getCityJSON(fileName: String = "popular_city_list.json"): String {
        val inputStream = getInputStreamForJsonFile(fileName)
        return inputStream.bufferedReader().use { it.readText() }
    }

    @Throws(IOException::class)
    internal fun getInputStreamForJsonFile(fileName: String): InputStream {
        return ClimateApp.appContext.assets.open(fileName)
    }
}
