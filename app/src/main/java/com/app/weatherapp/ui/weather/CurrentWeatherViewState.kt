package com.app.weatherapp.ui.weather

import com.app.weatherapp.base.BaseViewState
import com.app.weatherapp.db.entity.CurrentWeatherEntity
import com.app.weatherapp.domain.Status

class CurrentWeatherViewState(
    val status: Status,
    val error: String? = null,
    val data: CurrentWeatherEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}
