package com.app.weatherapp.ui.splash

import android.graphics.Color
import androidx.navigation.fragment.findNavController
import com.app.weatherapp.R
import com.app.weatherapp.base.BaseFragment
import com.app.weatherapp.constants.Constants
import com.app.weatherapp.databinding.FragmentSplashBinding
import com.app.weatherapp.di.Injectable
import com.app.weatherapp.utils.extensions.hide
import com.app.weatherapp.utils.extensions.show
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.backgroundColor
import com.mikhaellopez.rxanimation.fadeIn
import com.mikhaellopez.rxanimation.fadeOut
import io.reactivex.disposables.CompositeDisposable


class SplashFragment : BaseFragment<SplashFragmentViewModel, FragmentSplashBinding>(
    R.layout.fragment_splash,
    SplashFragmentViewModel::class.java
),
    Injectable {

    var disposable = CompositeDisposable()

    override fun init() {
        super.init()

        if (binding.viewModel?.sharedPreferences?.getString(Constants.Coordinates.LON, "")
                .isNullOrEmpty()
        ) {
            binding.buttonExplore.show()
            binding.viewModel?.navigateDashboard = false
        } else {
            binding.buttonExplore.hide()
            binding.viewModel?.navigateDashboard = true
        }

        binding.viewModel?.navigateDashboard?.let { startSplashAnimation(it) }

        binding.buttonExplore.setOnClickListener {
            binding.viewModel?.navigateDashboard?.let { it1 -> endSplashAnimation(it1) }
        }

    }

    private fun startSplashAnimation(navigateToDashboard: Boolean) {
        disposable.add(
            RxAnimation.sequentially(
                binding.buttonExplore.fadeIn(1000L)
            ).doOnTerminate {
                findNavController().graph.startDestination = R.id.weatherFragment
                if (navigateToDashboard)
                    endSplashAnimation(navigateToDashboard)
            }
                .subscribe()
        )
    }

    private fun endSplashAnimation(navigateToDashboard: Boolean) {
        disposable.add(
            RxAnimation.sequentially(
                binding.buttonExplore.fadeOut(300L),
                binding.rootView.backgroundColor(
                    Color.parseColor("#5D50FE"),
                    Color.parseColor("#FFFFFF"),
                    duration = 750L
                )
            )
                .doOnTerminate {
                    findNavController().graph.startDestination = R.id.weatherFragment
                    if (navigateToDashboard)
                    navigate(R.id.action_splashFragment_to_weatherFragment)
                    else
                        navigate(R.id.action_splashFragment_to_locationFragment)
                }
                .subscribe()

        )
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
