package com.app.weatherapp.ui.weather

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.weatherapp.R
import com.app.weatherapp.base.BaseFragment
import com.app.weatherapp.constants.Constants
import com.app.weatherapp.databinding.FragmentWeatherBinding
import com.app.weatherapp.di.Injectable
import com.app.weatherapp.domain.model.ListItem
import com.app.weatherapp.domain.usecase.CurrentWeatherUseCase
import com.app.weatherapp.domain.usecase.ForecastUseCase
import com.app.weatherapp.ui.weather.forecast.ForecastAdapter
import com.app.weatherapp.utils.extensions.isNetworkAvailable
import com.app.weatherapp.utils.extensions.observeWith

class WeatherFragment : BaseFragment<WeatherFragmentViewModel, FragmentWeatherBinding>(
    R.layout.fragment_weather,
    WeatherFragmentViewModel::class.java
), Injectable {

    override fun init() {
        super.init()
        initForecastAdapter()

        val lat: String =
            binding.viewModel?.sharedPreferences?.getString(Constants.Coordinates.LAT, "")!!
        val lon: String =
            binding.viewModel?.sharedPreferences?.getString(Constants.Coordinates.LON, "")!!

        if (lat.isNotEmpty() && lon.isNotEmpty()) {
            binding.viewModel?.setCurrentWeatherParams(
                CurrentWeatherUseCase.CurrentWeatherParams(
                    lat,
                    lon,
                    isNetworkAvailable(requireContext()),
                    Constants.Coordinates.METRIC
                )
            )
            binding.viewModel?.setForecastParams(
                ForecastUseCase.ForecastParams(
                    lat,
                    lon,
                    isNetworkAvailable(requireContext()),
                    Constants.Coordinates.METRIC
                )
            )
        }

        binding.viewModel?.getForecastViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                viewState = it
                it.data?.weather?.let { forecasts -> initForecast(forecasts) }
                binding.containerForecast.tvToday.text =
                    it.data?.city?.getCityAndCountry().plus(" ").plus("Todays Weather")
            }
        }

        binding.viewModel?.getCurrentWeatherViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                containerForecast.viewState = it
            }
        }

        binding.containerForecast.imgLocation.setOnClickListener {
            findNavController().navigate(R.id.action_weatherFragment_to_locationFragment)
        }
    }

    private fun initForecastAdapter() {
        val adapter = ForecastAdapter()
        binding.recyclerForecast.adapter = adapter
        binding.recyclerForecast.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        postponeEnterTransition()
        binding.recyclerForecast.viewTreeObserver
            .addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
    }

    private fun initForecast(list: List<ListItem>) {
        (binding.recyclerForecast.adapter as ForecastAdapter).submitList(list)
    }
}
