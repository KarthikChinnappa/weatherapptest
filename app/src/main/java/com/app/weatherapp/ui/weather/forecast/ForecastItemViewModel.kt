package com.app.weatherapp.ui.weather.forecast

import androidx.databinding.ObservableField
import com.app.weatherapp.base.BaseViewModel
import com.app.weatherapp.domain.model.ListItem
import com.app.weatherapp.domain.model.WeatherItem
import javax.inject.Inject


class ForecastItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<ListItem>()
}
