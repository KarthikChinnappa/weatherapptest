package com.app.weatherapp.ui.location

import androidx.navigation.fragment.findNavController
import com.app.weatherapp.R
import com.app.weatherapp.base.BaseFragment
import com.app.weatherapp.databinding.FragmentLocationBinding
import com.app.weatherapp.db.entity.City
import com.app.weatherapp.di.Injectable
import com.utsman.recycling.setupAdapter
import kotlinx.android.synthetic.main.item_city.view.*

class LocationFragment : BaseFragment<LocationViewModel, FragmentLocationBinding>(
    R.layout.fragment_location,
    LocationViewModel::class.java
),
    Injectable {

    override fun init() {
        super.init()
        initSearchResultsAdapter()
    }

    private fun initSearchResultsAdapter() {

        binding.recyclerViewSearchResults.setupAdapter<City>(R.layout.item_city) { adapter, context, list ->
            bind { itemView, position, item ->
                itemView.tvCityName.text = item?.city
                itemView.setOnClickListener {
                    binding.viewModel?.saveCoordsToSharedPref(item!!)
                        ?.subscribe { _, _ ->
                            findNavController().navigate(R.id.action_locationFragment_to_weatherFragment)
                        }
                }
            }

            submitList(binding.viewModel!!.getTopCities())

        }
        /* val adapter = SearchResultAdapter { item ->
             binding.viewModel?.saveCoordsToSharedPref(item.coord!!)
            // findNavController().navigate(R.id.action_searchFragment_to_dashboardFragment)
         }

         binding.recyclerViewSearchResults.adapter = adapter
         binding.recyclerViewSearchResults.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
 */
    }
}

