package com.app.weatherapp.ui.main

import com.app.weatherapp.base.BaseViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject internal constructor() : BaseViewModel() {}
