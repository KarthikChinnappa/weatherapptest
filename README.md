# Project Title

WeatherApp

#Instruction
1.Clone this project using the bit-bucket url
2.Run the project phone/tablet with minimum SDK version:26
3.Initial Splash screen click lets start button
4.Choose city location
5.Showing City current and forecast weather list

#What more you can do if you have more time to implement?
1.Splash will have dynamic animation of Lottie file based on the current weather of the user
2.Transition of screen can have better smooth
3.Weather card UI enchantment
4.Implement the location service to get Lat/Long of the user to get the accurate location
5.Local data encryption can be added to have secure over the data
6.Can add search to choose the city/location to show the descried any place weather
7.Can add the Unit Tests.
